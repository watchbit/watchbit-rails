require 'faraday'

module Watchbit

  class Client
    attr_accessor :token

    def initialize(token, opts={})
      self.token = token
      self.api_endpoint = opts.delete(:api_endpoint) || "http://localhost:3100"
    end
    
    def post_metrics(metrics)
      connection.post do |req|
        req.options.timeout = 2
        req.options.open_timeout = 2
        req.url '/api/metrics'
        req.headers['X-AUTH-TOKEN'] = self.token
        req.headers['Content-Type'] = 'application/json'
        req.body = {measurements: metrics}.to_json
      end
    end

    def connection
      @connection ||= ::Faraday.new
    end
    
    def api_endpoint
      connection.url_prefix
    end

    def api_endpoint=(value)
      connection.url_prefix = value
    end
  end

end
