module Watchbit
  module Rails

    class Scheduller
      attr_accessor :thread, :period
      
      def initialize(period, &block)
        self.period = period
        @proc = block
        @first_run = true

        self.thread = Thread.new { run }
      end

      protected

      def perform
        @proc.call if @proc
      rescue => e
      end

      def run
        loop do
          compersated_period = if @first_run
            @first_run = false
            period
          else
            start = Time.now
            perform
            stop = Time.now
            period - (stop-start)
          end
          sleep(compersated_period) if compersated_period > 0
        end
      end
    end

  end
end
