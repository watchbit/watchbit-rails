require 'watchbit/client'
require 'watchbit/aggregator'
require_relative 'scheduller'

module Watchbit
  module Rails

    class Tracker
      attr_accessor :aggregator, :scheduller, :client

      delegate :inc, :push, to: :aggregator, allow_nil: true

      def initialize(token)
        self.client = ::Watchbit::Client.new(token)
        self.aggregator = ::Watchbit::Aggregator.new
      end

      def start_scheduller(period=nil)
        self.scheduller ||= Scheduller.new(period || 60) { sync }
      end

      def sync
        data = aggregator.reset.map do |key, value|
          {
            name: key,
            value: value,
            created_at: Time.now
          }
        end

        client.post_metrics(data) if data.present?
        # puts "post: #{data}" if data.present?
      rescue => e
        puts e.to_s
      end
    end
    
  end
end
