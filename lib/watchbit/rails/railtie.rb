module Watchbit
  module Rails

    class Railtie < ::Rails::Railtie
      config.watchbit = ::ActiveSupport::OrderedOptions.new
      config.watchbit.background_sync_period = 60

      initializer "watchbit-rails.tracker" do
        opts = config.watchbit

        next unless opts.enabled
        next unless opts.token

        require 'watchbit/rails/tracker'

        Watchbit.tracker = Watchbit::Rails::Tracker.new(opts.token)
        
        Watchbit.tracker.start_scheduller(opts.background_sync_period) if opts.background_sync
        Watchbit.tracker.client.api_endpoint = opts.api_endpoint if opts.api_endpoint
      end

      initializer "watchbit-rails.subscribers" do
        opts = config.watchbit
        next unless opts.enabled

        require 'watchbit/rails/subscriber'

        if opts.action_controller_subscriber
          require 'watchbit/rails/action_controller_subscriber'
          ActionControllerSubscriber.attach_to :action_controller
        end
      end
    end

  end
end
