module Watchbit
  module Rails

    class Subscriber < ::ActiveSupport::Subscriber
      protected
      
      def tracker
        Watchbit.tracker
      end

      def config
        ::Rails.application.config.watchbit
      end
    end

  end
end
