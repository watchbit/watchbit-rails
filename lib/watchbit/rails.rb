require_relative 'rails/version'
require_relative 'rails/railtie' if defined?(Rails)

module Watchbit
 
  def self.tracker
    @tracker
  end

  def self.tracker=(value)
    @tracker = value
  end

  module Rails
  end

end
