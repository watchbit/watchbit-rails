module Watchbit

  class Aggregator
    attr_accessor :values

    def initialize
      self.values = {}
    end

    def inc(key)
      synchronize do
        values[key] ||= 0
        values[key] += 1
      end
    end

    def push(key, value)
      synchronize do
        values[key] ||= []
        values[key] << value
      end
    end

    def reset
      synchronize do
        tmp = aggregate
        self.values = {}

        tmp
      end
    end

    protected
    
    def aggregate
      values.map do |key, value|
        case value
        when Numeric then [key, value]
        when Array then [key, percentile(value, 0.95)]
        end
      end.to_h
    end

    def percentile(values, percentile)
      if values.length > 1
        values_sorted = values.sort
        k = (percentile*(values_sorted.length-1)+1).floor - 1
        f = (percentile*(values_sorted.length-1)+1).modulo(1)

        return values_sorted[k] + (f * (values_sorted[k+1] - values_sorted[k]))
      else
        return values.first
      end
    end

    def synchronize
      semaphore.synchronize do
        yield
      end
    end

    def semaphore
      @semaphore ||= Mutex.new
    end
  end

end
